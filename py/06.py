from collections import deque

class Solution:
    def __init__(self, input_str, max_len):
        self.input_str = input_str
        self.max_len = int(max_len)

    def solve(self):
        i = 0
        q = deque()
        for char in iter(self.input_str):
            i += 1
            q.append(char)
            if len(q) > self.max_len:
                q.popleft()
            if len(set(q)) == self.max_len:
                return i
        return -1
