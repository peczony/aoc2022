import io


class Solution:
    values = ("rock", "paper", "scissors")
    mapping = {
        "A": "rock",
        "B": "paper",
        "C": "scissors",
        "X": "rock",
        "Y": "paper",
        "Z": "scissors",
    }
    winning_dict = {
        ("paper", "rock"): "paper",
        ("rock", "scissors"): "rock",
        ("paper", "scissors"): "scissors",
    }
    winners_and_losers = {
        "rock": {"to_win_against": "paper", "to_lose_against": "scissors"},
        "scissors": {"to_win_against": "rock", "to_lose_against": "paper"},
        "paper": {"to_win_against": "scissors", "to_lose_against": "rock"},
    }
    point_mapping = {"scissors": 3, "paper": 2, "rock": 1}

    def __init__(self, input_str, mode="1"):
        self.input_str = input_str
        self.mode = mode

    @classmethod
    def who_wins(cls, a, b):
        return cls.winning_dict[(min(a, b), max(a, b))]

    @classmethod
    def get_sides(cls, value):
        other_values = list(set(cls.values) - value)
        first_result = cls.who_wins(value, other_values[0])

    @classmethod
    def get_scoring(cls, me, opponent):
        my_points = cls.point_mapping[me]
        if me == opponent:
            round_points = 3
        elif cls.who_wins(me, opponent) == me:
            round_points = 6
        else:
            round_points = 0
        return my_points + round_points

    @classmethod
    def round(cls, opponent, me):
        opponent = cls.mapping[opponent]
        me = cls.mapping[me]
        return cls.get_scoring(me, opponent)

    @classmethod
    def round2(cls, opponent, me):
        opponent = cls.mapping[opponent]
        if me == "Y":
            me = opponent
        else:
            vals = cls.winners_and_losers[opponent]
            if me == "X":
                me = vals["to_lose_against"]
            elif me == "Z":
                me = vals["to_win_against"]
        return cls.get_scoring(me, opponent)

    def solve(self):
        score = 0
        for line_ in io.StringIO(self.input_str):
            line = line_.strip()
            sp = line.split()
            if len(sp) == 2:
                if self.mode == "1":
                    score += self.round(sp[0], sp[1])
                elif self.mode == "2":
                    score += self.round2(sp[0], sp[1])
        return score
