import io
from collections import Counter

class Solution:
    def __init__(self, input_str, top="1"):
        self.input_str = input_str
        self.top = int(top)

    def solve(self):
        id_ = 0
        cntr = Counter()
        for line_ in io.StringIO(self.input_str):
            line = line_.strip()
            if line:
                cntr[id_] += int(line)
            else:
                id_ += 1
        result = 0
        for tup in cntr.most_common(self.top):
            result += tup[1]
        return result
