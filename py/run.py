#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse
import importlib


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--module", "-m")
    parser.add_argument("--input", "-i")
    parser.add_argument("--additional_arg", "-a")
    args = parser.parse_args()

    spec = importlib.util.spec_from_file_location(
        "module", args.module
    )
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)

    if os.path.isfile(args.input):
        with open(args.input) as f:
            input_ = f.read()
    else:
        input_ = args.input

    solution_args = [input_]
    if args.additional_arg:
        solution_args.append(args.additional_arg)
    
    print(module.Solution(*solution_args).solve())



if __name__ == "__main__":
    main()
